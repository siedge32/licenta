from scipy import signal
import numpy as np


class ShortTimeFourierTransform:
    def __init__(self, sample_rate, window_size, overlap=0):
        self.sample_rate = sample_rate
        self.window_size = window_size
        self.overlap = overlap

    def stft(self, data):  # -> tuple[np.ndarray, np.ndarray, np.ndarray]:
        # overlap if its none, by default is nperseg//2
        # f = Array of sample frequencies. (frequency increment)
        # t = Array of segment times. (length of a frame in sec / 2 -> i guess its because of nyquist theorem)
        # zxx = STFT of x. By default, the last axis of Zxx corresponds to the segment times.
        f, t, zxx = signal.stft(data, self.sample_rate, nperseg=self.window_size)

        return f, t, zxx
