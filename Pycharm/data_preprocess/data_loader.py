import pandas as pd
import numpy as np

class DataLoader:
    def __init__(self, name):
        path = "..\\Bearing Vibration Image\\data_set_48k_csv\\" + name
        self.data = pd.read_csv(path)

    def get_data(self, column):
        return self.data[column].values

    def show_columns(self) -> np.ndarray:
        for column in self.data.columns:
            print(column)
