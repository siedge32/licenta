from data_preprocess.data_loader import DataLoader
from data_preprocess.fourier.stft import ShortTimeFourierTransform


def __main__():
    data_loader = DataLoader("0hp_normal_1796.csv")

    data = data_loader.get_data("X097_DE_time")

    sample_rate = 48000
    window_size = 2048

    stft = ShortTimeFourierTransform(sample_rate, window_size)
    f, t, zxx = stft.stft(data)


if __name__ == "__main__":
    __main__()
