-> The data contain vibration signals collected from bearings of different health conditions under time-varying rotational speed conditions.
-> There are 36 datasets in total. 
-> For each dataset, there are two experimental settings: bearing health condition and varying speed condition.
-> The health conditions of the bearing include:
	(i) healthy,
	(ii) faulty with an inner race defect,
	(iii) faulty with an outer race defect.
-> The operating rotational speed conditions are:
	(i) increasing speed,
	(ii) decreasing speed,
	(iii) increasing then decreasing speed,
	(iv) decreasing then increasing speed.
-> Therefore, there are 12 different cases for the setting.
-> To ensure the authenticity of the data, 3 trials are collected for each experimental setting which results in 36 datasets in total.
-> Each dataset contains two channels: 'Channel_1' is vibration data measured by the accelerometer and 'Channel_2' is the rotational speed data measured by the encoder.
-> All these data are sampled at 200,000Hz and the sampling duration is 10 seconds.

^
| 
That part is from this link: https://data.mendeley.com/datasets/v43hmbwxpm/1
The name of this data set is:
	"Bearing Vibration Data under Time-varying Rotational Speed Conditions" 

Our naming convention for the file will be following this template:
	[health_condition]_[rotation_type]_[trial_no].csv