# Licenta

[Condition monitoring + Anomaly detection (Towards Data Science Theory)](https://towardsdatascience.com/how-to-use-machine-learning-for-anomaly-detection-and-condition-monitoring-6742f82900d7)
- [Autoencoder networks](https://towardsdatascience.com/auto-encoder-what-is-it-and-what-is-it-used-for-part-1-3e5c6f017726)
- [PCA(Principal Component Analysis)](https://towardsdatascience.com/a-one-stop-shop-for-principal-component-analysis-5582fb7e0a9c)
- [Mahalanobis distance](https://en.wikipedia.org/wiki/Mahalanobis_distance)
- [Code implementation](https://towardsdatascience.com/machine-learning-for-anomaly-detection-and-condition-monitoring-d4614e7de770)


- [General Ideea of what is suposed to be, different approches](https://medium.com/bigdatarepublic/machine-learning-for-predictive-maintenance-where-to-start-5f3b7586acfb)
- [Evaluating Failure Prediction Models for Predictive Maintenance](https://blogs.technet.microsoft.com/machinelearning/2016/04/19/evaluating-failure-prediction-models-for-predictive-maintenance/)
- [Predicting machine failure](https://www.machinedesign.com/markets/predicting-machine-failure)
- [Code Example, code from IBM](https://github.com/IBM/iot-predictive-analytics)