# Try to replicate the tests from this [article]()

- [Results](https://tinyurl.com/r8o368a) so far.

## Project Data
- [Data set](https://csegroups.case.edu/bearingdatacenter/pages/download-data-file) in tree forms (raw data, CSV of raw data, CSV for image data)


## Jupyter notebook files
- data_set_extractor -> extracts the raw .mat file into .csv.
- image_construct -> constructs the data set for the CNN, gets 400 values from previous .csv and scales [0, 1] to make a 20x20 image. Saves the image into another .csv.
- train_test_file_construct -> constructs the final .csv with all types of faults.

## Other files
- In doc folder it's a copy of the article.